#ifndef N_GAMESTATE_H
#define N_GAMESTATE_H

#include "application/nappstate.h"
#include "scene/nrendercontext.h"
#include "scene/ntransformnode.h"

//------------------------------------------------------------------------------
class nGameState : public nAppState
{
public:
	nGameState();

    /// called when state is becoming active
    virtual void OnStateEnter(const nString& prevState);
    /// called on state to perform state logic 
    virtual void OnFrame();
    /// called on state to perform 3d rendering
    virtual void OnRender3D();
    /// called on state to perform 2d rendering
    virtual void OnRender2D();

protected:
	virtual void TransferGlobalVariables();

    uint frameId;
    nRenderContext renderContext;
	nRef<nTransformNode> refRootNode;
};

//------------------------------------------------------------------------------
#endif
