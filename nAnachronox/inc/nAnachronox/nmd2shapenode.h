#ifndef N_MD2SHAPENODE_H
#define N_MD2SHAPENODE_H
//------------------------------------------------------------------------------
/**
    @class nMD2ShapeNode
    @ingroup SceneNodes

    @brief A shape node representing.

    See also @ref N2ScriptInterface_nmd2shapenode

    (C) 2004 RadonLabs GmbH
*/
#include "scene/nmaterialnode.h"
#include "gfx2/ndynamicmesh.h"
#include "nAnachronox/nmd2model.h"
#include "scene/nshapenode.h"

class nRenderContext;

//------------------------------------------------------------------------------
class nMD2ShapeNode : public nMaterialNode
{
public:
    /// constructor
    nMD2ShapeNode();
    /// destructor
    virtual ~nMD2ShapeNode();

	/// load resources
    virtual bool LoadResources();
    /// unload resources
    virtual void UnloadResources();

	/// called by app when new render context has been created for this object
    virtual void RenderContextCreated(nRenderContext* renderContext);
    /// indicate to scene server that we offer geometry for rendering
    virtual bool HasGeometry() const;
    /// perform pre-instancing rending of geometry
    virtual bool ApplyGeometry(nSceneServer* sceneServer);
    /// render geometry
    virtual bool RenderGeometry(nSceneServer* sceneServer, nRenderContext* renderContext);
    /// update transform and render into scene server
    virtual bool RenderTransform(nSceneServer* sceneServer, nRenderContext* renderContext, const matrix44& parentMatrix);

    /// set the mesh group index
    void SetGroupIndex(int i);
    /// get the mesh group index
    int GetGroupIndex() const;

protected:
    /// load the model resource 
    bool LoadModel();
    /// unload the model resource
    void UnloadModel();

	nDynamicMesh dynMesh;
	nMD2Model* model;
    int groupIndex;

    nVariable::Handle timeHandle;
    nVariable::Handle windHandle;
};

//------------------------------------------------------------------------------
/**
*/
inline
void
nMD2ShapeNode::SetGroupIndex(int i)
{
    this->groupIndex = i;
}

//------------------------------------------------------------------------------
/**
*/
inline
int
nMD2ShapeNode::GetGroupIndex() const
{
    return this->groupIndex;
}

//------------------------------------------------------------------------------
#endif
