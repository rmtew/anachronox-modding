#ifndef N_ANACHRONOX_H
#define N_ANACHRONOX_H

#include "application/napplication.h"
#include "application/nappcamera.h"
#include "nanachronox/ngamestate.h"
#include "nanachronox/nanoxcamera.h"
#include "mathlib/vector.h"
#include "mathlib/transform44.h"

class nAnachronox : public nApplication
{
public:
    virtual bool Open();
    /// create camera object, overrides superclass.
    virtual nAppCamera* CreateAppCamera();
	virtual nAnoxCamera* GetCamera();

protected:
    /// create script server object, override in subclass as needed
    virtual nScriptServer* CreateScriptServer();

    nGameState*     gameState;
	nAnoxCamera*    camera;
};

#endif