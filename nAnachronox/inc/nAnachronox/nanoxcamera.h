#ifndef N_ANOXCAMERA_H
#define N_ANOXCAMERA_H
//------------------------------------------------------------------------------
/**
    @class nAnoxCamera
    @ingroup nAnachronox
*/
#include "kernel/nroot.h"
#include "application/nappcamera.h"

//------------------------------------------------------------------------------
class nAnoxCamera : public nRoot
{
public:
    /// constructor
    nAnoxCamera();
    /// destructor
    ~nAnoxCamera();

	virtual nAppCamera* GetAppCamera();

protected:
	nAppCamera *camera;
};

//------------------------------------------------------------------------------
#endif
