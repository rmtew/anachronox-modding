#line 1 "grass_lightmapped.fx"
//------------------------------------------------------------------------------
//  grass_lightmapped.fx
//
//  The default shader for dx7 cards using fixed function pipeline,
//  running in 1 render pass.
//
//  (C) 2003 RadonLabs GmbH
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  The technique.
//------------------------------------------------------------------------------
technique t0
{
    pass p0
    {
        // empty
    }
}
