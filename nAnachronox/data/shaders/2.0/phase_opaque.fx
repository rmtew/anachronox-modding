#line 1 "phase_opaque.fx"
//------------------------------------------------------------------------------
//  phase_opaque.fx
//
//  Set render states which are constant for the entire opaque phase in
//  the color pass.
//
//  (C) 2004 RadonLabs GmbH
//------------------------------------------------------------------------------
technique t0
{
    pass p0
    {
        ZWriteEnable     = true;
        AlphaBlendEnable = false;
        AlphaTestEnable  = true;
        AlphaFunc = GreaterEqual;
    }
}
