#include "nanachronox/nanachronox.h"
#include "application/nappcamera.h"

static void n_getposition(void* slf, nCmd* cmd);
static void n_geteulerrotation(void* slf, nCmd* cmd);
static void n_seteulerrotation(void* slf, nCmd* cmd);
static void n_gettranslation(void* slf, nCmd* cmd);
static void n_settranslation(void* slf, nCmd* cmd);

void
n_initcmds(nClass* cl)
{
    cl->BeginCmds();
    cl->AddCmd("fff_getposition_v",  'GPOS', n_getposition);
    cl->AddCmd("fff_geteulerrotation_v",  'GERT', n_geteulerrotation);
    cl->AddCmd("v_seteulerrotation_fff",  'SERT', n_seteulerrotation);
    cl->AddCmd("fff_gettranslation_v",  'GTRN', n_gettranslation);
    cl->AddCmd("v_settranslation_fff",  'STRN', n_settranslation);
    cl->EndCmds();
}

//------------------------------------------------------------------------------
/**
    @cmd
    getposition
    @input
    v
    @output
    fff
    @info
	xxx
*/
static void
n_getposition(void* slf, nCmd* cmd)
{
    nAnoxCamera* self = (nAnoxCamera*) slf;
    const matrix44& matrix = self->GetAppCamera()->GetViewMatrix();
    cmd->Out()->SetF(matrix.x_component().x);
    cmd->Out()->SetF(matrix.y_component().y);
    cmd->Out()->SetF(matrix.z_component().z);
}

//------------------------------------------------------------------------------
/**
    @cmd
    geteulerrotation
    @input
    v
    @output
    fff
    @info
	xxx
*/
static void
n_geteulerrotation(void* slf, nCmd* cmd)
{
    nAnoxCamera* self = (nAnoxCamera*) slf;
	transform44& transform = self->GetAppCamera()->Transform();
	const vector3& vector = transform.geteulerrotation();
    cmd->Out()->SetF(vector.x);
    cmd->Out()->SetF(vector.y);
    cmd->Out()->SetF(vector.z);
}

//------------------------------------------------------------------------------
/**
    @cmd
    seteulerrotation
    @input
    fff
    @output
    v
    @info
	xxx
*/
static void
n_seteulerrotation(void* slf, nCmd* cmd)
{
    nAnoxCamera* self = (nAnoxCamera*) slf;
	transform44& transform = self->GetAppCamera()->Transform();
    vector3 rotation = (vector3&)transform.geteulerrotation();
	rotation.x = cmd->In()->GetF();
	rotation.y = cmd->In()->GetF();
	rotation.z = cmd->In()->GetF();
	transform.seteulerrotation(rotation);
}

//------------------------------------------------------------------------------
/**
    @cmd
    gettranslation
    @input
    v
    @output
    fff
    @info
	xxx
*/
static void
n_gettranslation(void* slf, nCmd* cmd)
{
    nAnoxCamera* self = (nAnoxCamera*) slf;
	transform44& transform = self->GetAppCamera()->Transform();
	const vector3& vector = transform.gettranslation();
    cmd->Out()->SetF(vector.x);
    cmd->Out()->SetF(vector.y);
    cmd->Out()->SetF(vector.z);
}

//------------------------------------------------------------------------------
/**
    @cmd
    settranslation
    @input
    fff
    @output
    v
    @info
	xxx
*/
static void
n_settranslation(void* slf, nCmd* cmd)
{
    nAnoxCamera* self = (nAnoxCamera*) slf;
	transform44& transform = self->GetAppCamera()->Transform();
    vector3 translation = (vector3&)transform.gettranslation();
	translation.x = cmd->In()->GetF();
	translation.y = cmd->In()->GetF();
	translation.z = cmd->In()->GetF();
	transform.settranslation(translation);
}
