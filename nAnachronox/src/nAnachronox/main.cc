#include "kernel/nwin32loghandler.h"
#include "tools/nwinmaincmdlineargs.h"
#include "kernel/nkernelserver.h"
#include "kernel/nfileserver2.h"
#include "application/napplication.h"

#include "nanachronox/nanachronox.h"

nNebulaUsePackage(nnebula);
nNebulaUsePackage(nanachronox);
nNebulaUsePackage(napplication);
nNebulaUsePackage(ndinput8);
nNebulaUsePackage(ndirect3d9);
nNebulaUsePackage(ndshow);
nNebulaUsePackage(ngui);
nNebulaUsePackage(nnetwork);
nNebulaUsePackage(npythonserver);
nNebulaUsePackage(ndsaudioserver3);

void
nPythonRegisterPackages(nKernelServer * kernelServer)
{       
    kernelServer->AddPackage(nnebula);
    kernelServer->AddPackage(napplication);
    kernelServer->AddPackage(ndinput8);
    kernelServer->AddPackage(ndirect3d9);
    kernelServer->AddPackage(ndshow);
    kernelServer->AddPackage(ngui);
    kernelServer->AddPackage(nnetwork);
    kernelServer->AddPackage(npythonserver);
    kernelServer->AddPackage(nanachronox);
    kernelServer->AddPackage(ndsaudioserver3);
}       

#ifdef __WIN32__x
int APIENTRY WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
#else
int main( int argc, char * argv[] )
#endif
{
#ifdef __WIN32__x
    char buffer[N_MAXPATH];
    GetModuleFileName( 0, buffer, sizeof(buffer) );
    nString exePath( buffer );
    exePath.ConvertBackslashes();
#else
    nString exePath( argv[0] );
#endif
    nString gameName = exePath.ExtractFileName();
    gameName.StripExtension();

	nKernelServer kernelServer;
    // nWin32LogHandler logHandler("nanachronox");
    //kernelServer.SetLogHandler(&logHandler);

	nPythonRegisterPackages(&kernelServer);

	kernelServer.GetFileServer()->SetAssign("proj", kernelServer.GetFileServer()->GetAssign("home"));

    nAnachronox *game = (nAnachronox *)kernelServer.New("nanachronox", "/game");

	nDisplayMode2 displayMode;
	displayMode.SetType(nDisplayMode2::Type::Windowed);
	displayMode.SetWidth(800);
	displayMode.SetHeight(600);
	displayMode.SetWindowTitle("Anachronox");
    game->SetDisplayMode(displayMode);

	game->SetStartupScript("home:export/startup.py");

    if (game->Open()) {
        game->Run();
    }

    game->Close();
    game->Release();

	return 0;
}
