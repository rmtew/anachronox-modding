
#include "nanachronox/nmd2shapenode.h"
#include "kernel/npersistserver.h"

static void n_setgroupindex(void* slf, nCmd* cmd);
static void n_getgroupindex(void* slf, nCmd* cmd);

//------------------------------------------------------------------------------
/**
    @scriptclass
    nmd2shapenode

    @cppclass
    nMD2ShapeNode
    
    @superclass
    nshapenode

    @classinfo
    ...
*/
void
n_initcmds(nClass* cl)
{
    cl->BeginCmds();

	cl->AddCmd("v_setgroupindex_i",         'SGRI', n_setgroupindex);
    cl->AddCmd("i_getgroupindex_v",         'GGRI', n_getgroupindex);

    cl->EndCmds();
}

//------------------------------------------------------------------------------
/**
    @cmd
    setgroupindex
    @input
    i(GroupIndex)
    @output
    v
    @info
    Set the group index inside the mesh resource.
*/
static void
n_setgroupindex(void* slf, nCmd* cmd)
{
    nMD2ShapeNode* self = (nMD2ShapeNode*) slf;
    self->SetGroupIndex(cmd->In()->GetI());
}

//------------------------------------------------------------------------------
/**
    @cmd
    getgroupindex
    @input
    v
    @output
    i(GroupIndex)
    @info
    Get the group index inside the mesh resource.
*/
static void
n_getgroupindex(void* slf, nCmd* cmd)
{
    nMD2ShapeNode* self = (nMD2ShapeNode*) slf;
    cmd->Out()->SetI(self->GetGroupIndex());
}
