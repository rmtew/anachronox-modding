
#include "nanachronox/nanachronox.h"
#include "kernel/nkernelserver.h"

nNebulaScriptClass(nAnachronox, "napplication");

bool
nAnachronox::Open() {
    nApplication::Open();

    // create states.
    this->gameState = (nGameState*)this->CreateState("ngamestate", "game");

    // we specify the first state by menu state.
    this->SetState("game");

	return true;
}

//------------------------------------------------------------------------------
/**
*/
nAppCamera*
nAnachronox::CreateAppCamera()
{
	this->camera = (nAnoxCamera *)kernelServer->New("nanoxcamera", "/usr/cameras/default");
    return camera->GetAppCamera();
}

//------------------------------------------------------------------------------
/**
*/
nAnoxCamera*
nAnachronox::GetCamera()
{
    return this->camera;
}

//------------------------------------------------------------------------------
/** 
*/
nScriptServer*
nAnachronox::CreateScriptServer() {
    return (nScriptServer*) kernelServer->New("npythonserver", "/sys/servers/script");
}

////------------------------------------------------------------------------------
///** 
//*/
//void
//nAnachronox::SetView(float theta, float rho, float x, float y, float z) {
//    transform44 *viewTransform = &this->camera->Transform();
//	vector3 viewerPos(x, y, z);
//
//	matrix44 m;
//	m.ident();
//	m.rotate_x(theta);
//	m.rotate_y(rho);
//	m.translate(viewerPos);
//
//	viewTransform->setmatrix(m);
//}