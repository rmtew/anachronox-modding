//------------------------------------------------------------------------------
//  napplication_cmds.cc
//  (C) 2004 RadonLabs GmbH
//------------------------------------------------------------------------------
#include "nanachronox/nanachronox.h"

static void n_setcamera(void* slf, nCmd* cmd);

//------------------------------------------------------------------------------
/**
    @scriptclass
    nanachronox
    @cppclass
    nAnachronox
    @superclass
    nApplication
    @classinfo
    xxx
*/
void
n_initcmds(nClass* cl)
{
    cl->BeginCmds();
    cl->AddCmd("v_setcamera_s",  'STVW', n_setcamera);
    cl->EndCmds();
}

//------------------------------------------------------------------------------
/**
    @cmd
    setcamera
    @input
    s
    @output
    v
    @info
	xxx
*/
static void
n_setcamera(void* slf, nCmd* cmd)
{
    const char* s0 = cmd->In()->GetS();
    nAnachronox* self = (nAnachronox*) slf;
    // self->SetView(theta, rho, x, y, z);
}
