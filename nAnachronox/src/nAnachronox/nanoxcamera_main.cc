#include "kernel/nkernelserver.h"
#include "nanachronox/nanoxcamera.h"

nNebulaScriptClass(nAnoxCamera, "nroot");

//------------------------------------------------------------------------------
/**
*/
nAnoxCamera::nAnoxCamera()
{
    this->camera = new nAppCamera;
}

//------------------------------------------------------------------------------
/**
*/
nAnoxCamera::~nAnoxCamera()
{
    // empty
}

//------------------------------------------------------------------------------
/**
*/
nAppCamera*
nAnoxCamera::GetAppCamera()
{
    return this->camera;
}
