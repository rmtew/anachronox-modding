# TODO: "console %f", xxx
# the place where I normally find 0xffff after an expression is 0x0500 followed by the string "xxx"
# This implies that the 0xffff is actually an indication of combination.
# Need to read it before finishing an expression.

import os, sys
import struct
from StringIO import StringIO
from anoxtools import APE

# Syntax: decompile <file>

# All these values are
bitHeader = (317, 0xFFFFFFFFL)

def PrintHelp():
    fileName = os.path.split(sys.argv[0])[-1]
    print fileName, "<filename>"

def DecompileFile(fileName):
    try:
        f = open(fileName, 'rb')
    except IOError:
        PrintHelp()
        print "** File not found:", fileName
        sys.exit()

    try:
        v = struct.unpack("LL", f.read(8))
        if v == bitHeader:
           args = APE.APEDecompiler(f)
           APE.OutputAPECode(args.windows, args.switches, args.switchesByLabel)
        else:
            print "** Not an APE file:", fileName, hex(v)
    except:
        f.close()
        raise

    f.close()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        PrintHelp()
        sys.exit()

    arg = sys.argv[1]
    if arg == "--all":
        # Ensure the "ape\" directory exists.
        if os.access("ape", os.F_OK) != 1:
            print os.path.split(sys.argv[0])[-1] +": ape sub-directory not found.."
            sys.exit()
        # Ensure the "apes\" directory exists.
        if os.access("apes", os.F_OK) != 1:
            os.makedirs("apes")

        oldStdout = sys.stdout
        for fileName in os.listdir("ape"):
            if fileName.endswith(".ape"):
                oldStdout.write(fileName +".. ")
                # Install our own stdout replacement.
                out = sys.stdout = StringIO()
                # Decompile the file catching the output.
                try:
                    DecompileFile("ape\\"+ fileName)
                except:
                    print
                    print "#### ERROR ERROR ERROR ####"
                    oldStdout.write(out.getvalue())
                    raise
                s = out.getvalue()
                out.close()
                oldStdout.write(".. %s\n" % len(s))

                f = open("apes\\"+ fileName[:-4] +".txt", 'w')
                f.write(s)
                f.close()
        sys.stdout = oldStdout
    else:
        DecompileFile(arg)
