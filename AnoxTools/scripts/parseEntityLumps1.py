import os, sys
import StringIO

anoxdataPath = "C:\\Games\\Anachronox\\anoxdata"
mapsPath = os.path.join(anoxdataPath, "MAPS")

if len(sys.path[0]):
    # Executed from the command line.
    outPath = sys.path[0]
else:
    # Executed from some windows thing.
    outPath = os.getcwd()

# Make the output sub-directory.
outPath = os.path.join("entities-parse")
try:
    os.mkdir(outPath)
except OSError:
    pass

from anoxtools import BSP

for filename in os.listdir(mapsPath):
    if not filename.endswith(".bsp"):
        continue
    f = open(os.path.join(mapsPath, filename), 'rb')
    try:
        reader = BSP.BSPReader(f, "blah")
        reader.ParseEntities(f)
    finally:
        f.close()

    s = ""
    outFilename = filename[:-4] +".html"

    s += "<html>"
    s += "<body>"
    s += "<h1>File: %s</h1>" % filename
    for entry in reader.entryList:
        s += entry.HTML()
    s += "</body>"
    s += "</html>"

    if len(s):
        f = open(os.path.join(outPath, outFilename), "w")
        try:
            f.write(s)
        finally:
            f.close()        
        